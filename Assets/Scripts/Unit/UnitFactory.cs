﻿using System.Collections.Generic;
using Controller;

namespace Unit
{
    public class UnitFactory
    {
        private List<ISpawner> objectsToSpawn;

        public UnitFactory(List<ISpawner> objectsToSpawn)
        {
            this.objectsToSpawn = objectsToSpawn;
        }

        public void SpawnList()
        {
            objectsToSpawn.ForEach(x => x.Spawn());
        }
    }
}