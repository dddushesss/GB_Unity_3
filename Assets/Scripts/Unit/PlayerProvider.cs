﻿using System;
using Controller;
using Interfaces;
using JetBrains.Annotations;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Object = System.Object;

namespace Unit
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D), typeof(SpriteRenderer))]
    public class PlayerProvider : MonoBehaviour, IUnit
    {
        [SerializeField] private float speed;
        [SerializeField] private float jumpForce;
        [SerializeField] private float speedInAir;
        [SerializeField] private float mass;
        [SerializeField] private LayerMask ground;
        [SerializeField] private Vector3 spawnPoint;
        [SerializeField] private Canvas _canvas;
        private Text cointCountUI;
        private int coinCount = 0;
        private GameObject _gameObject;
        private Rigidbody2D _rigidbody2D;
        
        private void addCoin()
        {
            coinCount += 1;
            cointCountUI.text = coinCount.ToString();
        }
        

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<CoinProvider>() != null)
            {
                addCoin();
                GameObject.Destroy(other.gameObject);
            }

            else if (other.GetComponent<EnemyProvider>() != null)
            {
                Debug.Log("Умер");
                coinCount = 0;
                SceneManager.LoadScene("SampleScene");
            }
            
            else if (other.GetComponent<FinishProvider>())
            {
                SceneManager.LoadScene("SampleScene");
                other.GetComponent<FinishProvider>().EndGame();
            }
            
          
        }

        public Rigidbody2D Rigidbody
        {
            get
            {
                if (_rigidbody2D == null)
                {
                    _rigidbody2D =  GetComponent<Rigidbody2D>();
                }
                return _rigidbody2D;
            }
           
        }
        
        private Collider2D _collider2D;
        public Collider2D Collider2D
        {
            get
            {
                if (_collider2D == null)
                {
                    _collider2D = GetComponent<Collider2D>();
                }
                return _collider2D;
            }
        }

        public LayerMask Ground => ground;
        public float Speed => speed;
        public float JumpForce => jumpForce;
        public float SpeedInAir => speedInAir;
        public float Mass => mass;
       

        public void Move(Vector3 point)
        {
            throw new System.NotImplementedException();
        }

        public void StopMove()
        {
            throw new System.NotImplementedException();
        }

        public GameObject Spawn()
        {
            return GameObject.Instantiate(gameObject, spawnPoint, quaternion.identity);
        }

        public void OnEnable()
        {
            cointCountUI = GameObject.Instantiate(_canvas.gameObject).GetComponentInChildren<Text>();
            cointCountUI.text = coinCount.ToString();
        }

       
    }

    
}