﻿using Model;
using UnityEngine;

namespace Controller
{
    public interface IUnit : IMove
    {
        float Speed { get; }
        float JumpForce { get; }
        float SpeedInAir { get; }
        float Mass { get; }
        
    }
}