﻿using System;
using Controller;
using Data;
using Model;
using UnityEngine;

namespace Unit
{
    public class Player : IFixedExecute, ISpawner
    {
        private GameObject _gameObject;
        private PlayerProvider _data;
        private Collider2D _collider;
        private Rigidbody2D _rigidbody;

        public Player(PlayerData player)
        {
            _data = player.PlayerProvider;
        }

        public void FixedExecute(float deltaTime)
        {
            PlayerControl();
        }

        private void PlayerControl()
        {
            float move = Input.GetAxis(AxisManager.HORIZONTAL);
            float jump = Input.GetAxis(AxisManager.JUMP);

            if (move != 0 && _collider.IsTouchingLayers(_data.Ground))
                _rigidbody.velocity =
                    new Vector2(move * _data.Speed, _data.Rigidbody.velocity.y);

            else if (_collider.IsTouchingLayers(_data.Ground))
            {
                _rigidbody.velocity = Vector2.zero;
            }

            if (jump > 0 && _collider.IsTouchingLayers(_data.Ground))
            {
                _rigidbody.AddForce(new Vector2(_rigidbody.velocity.x, _data.JumpForce));
            }

            if (move != 0 && !_collider.IsTouchingLayers(_data.Ground))
            {
                _rigidbody.AddForce(new Vector2(_data.SpeedInAir * move, 0));
                if (Math.Abs(_rigidbody.velocity.x) >= _data.Speed)
                {
                    _rigidbody.velocity = new Vector2(_data.Speed * (Math.Abs(_rigidbody.velocity.x)/_rigidbody.velocity.x), _rigidbody.velocity.y);
                }
            }

            if (move == 0)
            {
                _rigidbody.velocity = new Vector2(0, _rigidbody.velocity.y);
            }
            
        }
        

        public GameObject Spawn()
        {
            _gameObject = GameObject.Instantiate(_data.gameObject);
            _collider = _gameObject.GetComponent<Collider2D>();
            _rigidbody = _gameObject.GetComponent<Rigidbody2D>();
            return _gameObject;
        }
        

        
    }
}