﻿using System.Collections.Generic;
using UnityEngine;

namespace Controller
{
    public interface ISpawner
    {
        GameObject Spawn();
    }
}