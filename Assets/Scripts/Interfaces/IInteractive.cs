﻿namespace Controller
{
    public interface IInteractive
    {
        void Interact();
    }
}