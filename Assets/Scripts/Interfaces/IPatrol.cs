﻿using UnityEngine;

namespace Model
{
    public interface IPatrol
    {
        Vector2 PointA { get; }
        Vector2 PointB { get; }
        void Patrol(Vector2 pointA, Vector2 pointB);
    }
}