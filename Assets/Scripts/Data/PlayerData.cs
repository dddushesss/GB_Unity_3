﻿using Unit;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Data/PlayerSettings")]
    public class PlayerData : ScriptableObject
    {
        [SerializeField] private GameObject _unitProvider;

        public PlayerProvider PlayerProvider => _unitProvider.GetComponent<PlayerProvider>();
        
    }
}