﻿using System.IO;
using Model;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "Data", menuName = "Custom/Data", order = 0)]
    public sealed class Data : ScriptableObject
    {
        
        [SerializeField] private string _levelsDataPath;
        [SerializeField] private string _playerDataPath;
        
        private LevelsData _levels;
        private PlayerData _player;

        public LevelsData Level
        {
            get
            {
                if (_levels == null)
                {
                    _levels = Load<LevelsData>("Data/" + _levelsDataPath);
                }

                return _levels;
            }
        }
        public PlayerData PlayerData
        {
            get
            {
                if (_player == null)
                {
                    _player = Load<PlayerData>("Data/" + _playerDataPath);
                }

                return _player;
            }
        }

        private T Load<T>(string resourcesPath) where T : Object =>
            Resources.Load<T>(Path.ChangeExtension(resourcesPath, null));
    }
}