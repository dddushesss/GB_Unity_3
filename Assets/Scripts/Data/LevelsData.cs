﻿using System.Collections.Generic;
using UnityEngine;

namespace Model
{
    [CreateAssetMenu(fileName = "LevelsSettings", menuName = "Custom/Data/LevelsSettings", order = 0)]
    public class LevelsData : ScriptableObject
    {
        [SerializeField] private List<GameObject> levels;

        public List<GameObject> GetLevelList()
        {
            return levels;
        }
    }
}