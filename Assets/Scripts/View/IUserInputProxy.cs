﻿using System;

namespace View
{
    public interface IUserInputProxy
    {
        event Action<float> AxisOnChange;
        
        void GetAxis();
    }
}