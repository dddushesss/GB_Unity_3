﻿using Model;
using View;

namespace Controller
{
    public class InputController : IExecute
    {
        public readonly IUserInputProxy _horizontal;
        public readonly IUserInputProxy _vertical;

        public InputController((IUserInputProxy horizontal, IUserInputProxy vertical) input)
        {
            _horizontal = input.horizontal;
            _vertical = input.vertical;
        }

        public void Execute(float deltaTime)
        {
            _horizontal.GetAxis();
            _vertical.GetAxis();
        }
    }
}