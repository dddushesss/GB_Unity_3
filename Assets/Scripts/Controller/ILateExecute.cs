﻿namespace Model
{
    public interface ILateExecute : IController
    {
        void LateExecute(float deltaTime);
    }
}