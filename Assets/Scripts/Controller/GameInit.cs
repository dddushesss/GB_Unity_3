﻿using System.Collections.Generic;
using Level;
using Model;
using Unit;
using UnityEngine;

namespace Controller
{
    public class GameInit
    {
        public GameInit(Controllers controllers, Data.Data data)
        {
            var inputInit = new InputInitialization();
            
            var levelFactory = new LevelFactory(data.Level);
            var player = new Player(data.PlayerData);
            
            var listToSpawn = new List<ISpawner>
            {
                levelFactory,
                player
            };

            var unitSpawner = new UnitFactory(listToSpawn);
            unitSpawner.SpawnList();
            controllers.Add(inputInit);
            var inputController =
                new InputController(inputInit.GetInput());
            controllers.Add(player);
            
            controllers.Add(inputInit);
           
            controllers.Add(inputController);
        }
    }
}