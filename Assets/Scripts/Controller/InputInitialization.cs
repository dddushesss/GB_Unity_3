﻿using Model;
using UnityEngine;
using View;

namespace Controller
{
    public class InputInitialization : IInitialsation
    {
        private IUserInputProxy _pcInputHorizontal;
        private IUserInputProxy _pcInputVertical;

        public InputInitialization()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                _pcInputHorizontal = new MobileInput();
            }
            else
            {
                _pcInputHorizontal = new PCInputHorizontal();
                _pcInputVertical = new PCInputVertical();
            }
        }

        public void Initialization()
        {
            
        }

        public (IUserInputProxy inputHorizontal, IUserInputProxy inputVertical) GetInput()
        {
            (IUserInputProxy inputHorizontal, IUserInputProxy inputVertical) result = (_pcInputHorizontal, _pcInputVertical);
            return result;
        }
    }
}