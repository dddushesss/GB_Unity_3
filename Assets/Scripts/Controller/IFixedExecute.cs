﻿using Model;

namespace Controller
{
    public interface IFixedExecute : IController
    {
        void FixedExecute(float deltaTime);
    }
}