﻿namespace Model
{
    public interface ICleanup : IController
    {
        void Cleanup();
    }
}