﻿using System;
using Model;
using UnityEngine;

namespace Controller
{
    public class CameraController : IExecute
    {
        private Camera _camera;
        private Transform _folowingObject;

        public CameraController(Camera camera, Transform folowingObject)
        {
            _camera = camera;
            _folowingObject = folowingObject;
        }

        private void UpdateCameraPosition() {
            try {
                _folowingObject.position = new Vector3(
                    _folowingObject.position.x, 
                    _folowingObject.position.y,
                    
                   _camera.transform.position.z 
                );
            } catch (Exception error) {
                
                Debug.LogError(error);
            }
        }
        
        public void Execute(float deltaTime)
        {
            UpdateCameraPosition();
        }
    }
}