﻿using System.Collections.Generic;
using System.Linq;
using Controller;
using Model;
using UnityEngine;

namespace Level
{
    public class LevelFactory : ISpawner
    {
        private readonly LevelsData _data;
        private List<GameObject> levelList;
        public LevelFactory(LevelsData data)
        {
            _data = data;
            levelList = new List<GameObject>();
            levelList.AddRange(_data.GetLevelList());
        }

        public GameObject Spawn()
        {
            var res = levelList.PopFirst();
            res.transform.localPosition = Vector3.zero;
            return Object.Instantiate(res);
        }

        
    }
}