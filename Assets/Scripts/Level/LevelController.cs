﻿using System.Collections.Generic;
using Interfaces;

namespace Level
{
    public class LevelController
    {
        private List<ISwitch> _listTOSwitch;

        public LevelController()
        {
            _listTOSwitch = new List<ISwitch>();
        }

        public void Add(ISwitch item)
        {
            _listTOSwitch.Add(item);
        }

        public void AddRange(List<ISwitch> list)
        {
            _listTOSwitch.AddRange(list);
        }

        public void SwitchLevel()
        {
            
        }
        
    }
}